<?php

namespace Nubex\CustomShippingRate\Block\Adminhtml\Order\Create\Shipping\Method;

class Form extends \Magento\Sales\Block\Adminhtml\Order\Create\Shipping\Method\Form
{

    /**
     * Custom shipping rate
     *
     * @return string
     */
    public function getActiveCustomShippingRateMethod()
    {
        $rate = $this->getActiveMethodRate();
        return $rate && $rate->getCarrier() == \Nubex\CustomShippingRate\Model\Carrier::CODE ? $rate->getMethod() : '';
    }

}
