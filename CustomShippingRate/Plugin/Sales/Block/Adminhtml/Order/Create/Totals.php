<?php

namespace Nubex\CustomShippingRate\Plugin\Sales\Block\Adminhtml\Order\Create;

class Totals
{

    public function aroundGetTotals(
        \Magento\Sales\Block\Adminhtml\Order\Create\Totals $subject, callable $proceed
    ) {
        $returnValue = $proceed();

        if (!($subject->getQuote()->isVirtual())){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $select = $connection->select()
                ->from(
                    ['o' => $connection->getTableName('quote_address')]
                )
                ->where('o.quote_id=?', $subject->getQuote()->getShippingAddress()->getQuoteId())
                ->where('o.address_type=?', 'shipping');
            $result = $connection->fetchAll($select);
            if(count($result) > 0)
                $quoteAddress = $result[0];

            $shipAmount = $quoteAddress['shipping_amount'];

            if($returnValue['shipping']->getValue() != $shipAmount){
                $returnValue['shipping']->setValue($shipAmount);
            }
        }



        return $returnValue;
    }

}
